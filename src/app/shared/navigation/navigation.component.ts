import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})

export class NavigationComponent implements OnInit {

  static LOGO_ROUTE_HOME    = '/home';
  static LOGO_ROUTE_BIG_NAV = '/big-navigation';

  logoRoute: string;

  constructor(private _router: Router) {

  }

  ngOnInit() {
    this.logoRoute = NavigationComponent.LOGO_ROUTE_HOME;
    console.log('Navigation loaded');
  }
  
  toggleLogoRoute() {
    this.logoRoute = NavigationComponent.LOGO_ROUTE_HOME;
    if (NavigationComponent.LOGO_ROUTE_HOME === this._router.url) {
        this.logoRoute = NavigationComponent.LOGO_ROUTE_BIG_NAV;
    } 
    
    this._router.navigate([this.logoRoute]);
  }

}
