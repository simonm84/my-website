import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-cv',
  templateUrl: './cv.component.html',
  styleUrls: ['./cv.component.css']
})
export class CvComponent implements OnInit {

  date;

  constructor() {
    this.date = new Date().toJSON().slice(0, 10).replace(/-/g, '/');
  }

  ngOnInit() {
  }

}
