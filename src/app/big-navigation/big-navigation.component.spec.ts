import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BigNavigationComponent } from './big-navigation.component';

describe('BigNavigationComponent', () => {
  let component: BigNavigationComponent;
  let fixture: ComponentFixture<BigNavigationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BigNavigationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BigNavigationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should be created', () => {
    expect(component).toBeTruthy();
  });
});
