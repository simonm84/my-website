import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { AppComponent } from './app.component';
import { CvComponent } from './cv/cv.component';
import { NavigationComponent } from './shared/navigation/navigation.component';
import { FooterComponent } from './shared/footer/footer.component';
import { HomeComponent } from './home/home.component';
import { BigNavigationComponent } from './big-navigation/big-navigation.component';

const routs = [
  { path: 'home', component: HomeComponent },
  { path: 'cv', component: CvComponent },
  { path: 'big-navigation', component: BigNavigationComponent },
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: '**', redirectTo: '/home', pathMatch: 'full' }
];

@NgModule({
  declarations: [
    AppComponent,
    CvComponent,
    NavigationComponent,
    FooterComponent,
    HomeComponent,
    BigNavigationComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routs)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
